package excelT

import (
	"fmt"
	"testing"
	
	"gitee.com/ydxj/gotls/baseT/testT"
)

func init() {
	testT.InitTestEnv()
}

func TestGetRows(t *testing.T) {
	e := ReadExcel("./test.xlsx")
	sheet := e.ReadSheetByIndex(0)
	for _, row := range sheet.ReadTitleRow() {
		for _, cell := range row.CellList {
			fmt.Print(cell.Position, "_", cell.Value, "|")
		}
		fmt.Println()
	}
	for _, cell := range sheet.GetMergeCells() {
		fmt.Println(cell.GetStartAxis(), cell.GetEndAxis(), cell.GetCellValue())
	}
}

func TestCopySheet(t *testing.T) {
	// 读取excel
	e1 := ReadExcel("./test.xlsx")
	e2 := CreateExcel("./test_copy.xlsx")
	// 读取sheet
	e1Sheet1 := e1.ReadSheetByIndex(0)
	e2Sheet1 := e2.ReadSheetByIndex(0)
	for _, row := range e1Sheet1.ReadRows() {
		for _, cell := range row.CellList {
			e2Sheet1.AddCellValueAndStyle(cell.RowIndex, cell.ColIndex, cell.Value, cell.GetStyle())
		}
	}
	for _, cell := range e1Sheet1.GetMergeCells() {
		e2Sheet1.SetMergeCells(cell.GetStartAxis(), cell.GetEndAxis())
	}
	e2.Save()
}

func TestWriterModel(t *testing.T) {

}
