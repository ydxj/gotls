package excelT

import (
	"fmt"
	"strconv"
	
	"github.com/xuri/excelize/v2"
	
	"gitee.com/ydxj/gotls/baseT/collection/sliceT"
)

// 获取索引
func GetCellPosition(row, col int) string {
	index, _ := excelize.CoordinatesToCellName(col, row)
	return index
}

func GetCellRowAndColIndex(position string) (row, col int) {
	row, col, _ = excelize.CellNameToCoordinates(position)
	return row, col
}

// 获取列索引
func GetColName(col int) string {
	colName, _ := excelize.ColumnNumberToName(col)
	return colName
}

// 获取列索引
func GetColIndex(col string) int {
	colIndex, _ := excelize.ColumnNameToNumber(col)
	return colIndex
}

// 将数据行转json
func RowsToJson(rows []*Row) []map[string]string {
	// 默认第一列是标题栏
	var keyList []string
	for _, cell := range rows[0].CellList {
		if sliceT.ContainStr(cell.Value, keyList) {
			panic("标题栏重复")
		} else {
			keyList = append(keyList, cell.Value)
		}
	}
	// 默认第二列开始是数据
	var dataList []map[string]string
	for rowIndex, row := range rows[1:] {
		if rowIndex%400 == 0 {
			fmt.Println(fmt.Sprintf("ToJson: Row Just : %d", rowIndex))
		}
		tempMap := make(map[string]string)
		tempMap["data.id"] = strconv.Itoa(rowIndex)
		for i, cell := range row.CellList {
			if i >= len(keyList) {
				continue
			}
			tempMap[keyList[i]] = cell.Value
		}
		dataList = append(dataList, tempMap)
	}
	return dataList
}

func RowsToJsonByKey(rows []*Row, keys ...string) map[string]map[string]string {
	// 默认第一列是标题栏
	var keyList []string
	for _, cell := range rows[0].CellList {
		if sliceT.ContainStr(cell.Value, keyList) {
			panic(fmt.Sprintf("标题栏重复 %s", cell.Value))
		} else {
			keyList = append(keyList, cell.Value)
		}
	}
	// 默认第二列开始是数据
	dataList := make(map[string]map[string]string)
	for rowIndex, row := range rows[1:] {
		tempMap := make(map[string]string)
		tempMap["data.id"] = strconv.Itoa(rowIndex)
		for i, cell := range row.CellList {
			if i >= len(keyList) {
				continue
			}
			tempMap[keyList[i]] = cell.Value
		}
		for _, key := range keys {
			if _, ok := tempMap[key]; !ok {
				panic("Key不存在")
			}
			dataList[tempMap[key]] = tempMap
		}
	}
	return dataList
}
