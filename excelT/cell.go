package excelT

import (
	"strconv"
	"strings"
	
	"github.com/xuri/excelize/v2"
)

// 单元格
type Cell struct {
	File      *excelize.File
	SheetName string
	Value     string
	RowIndex  int
	ColIndex  int
	Position  string
	// 通过方法读取
	type_      string
	style      *excelize.Style
	floatValue float64
}

// 获取单元格类型 - 应该通过cell.Style获取
func (cell *Cell) GetType() string {
	if cell.type_ == "" {
		cellType, _ := cell.File.GetCellType(cell.SheetName, cell.Position)
		switch cellType {
		case excelize.CellTypeNumber:
			cell.type_ = "Number"
		case excelize.CellTypeDate:
			cell.type_ = "Date"
		case excelize.CellTypeBool:
			cell.type_ = "Bool"
		case excelize.CellTypeUnset:
			cell.type_ = "Unset"
		case excelize.CellTypeFormula:
			cell.type_ = "Formula"
		case excelize.CellTypeInlineString:
			cell.type_ = "InlineString"
		case excelize.CellTypeError:
			cell.type_ = "Error"
		case excelize.CellTypeSharedString:
			cell.type_ = "String"
		default:
			cell.type_ = "Unknown"
		}
	}
	return cell.type_
}

// 获取浮点数
func (cell *Cell) GetFloatValue() float64 {
	if value, err := strconv.ParseFloat(strings.ReplaceAll(cell.Value, ",", ""), 64); err == nil {
		cell.floatValue = value
	} else {
		cell.floatValue = -123456789.1234
	}
	return cell.floatValue
}

// 读取单元格样式
func (cell *Cell) GetStyle() *excelize.Style {
	if cell.style == nil {
		cellStyleIndex, _ := cell.File.GetCellStyle(cell.SheetName, cell.Position)
		cell.style, _ = cell.File.GetStyle(cellStyleIndex)
	}
	return cell.style
}

// 设置单元格样式
func (cell *Cell) SetStyle(style *excelize.Style) {
	style_, _ := cell.File.NewStyle(style)
	_ = cell.File.SetCellStyle(cell.SheetName, cell.Position, cell.Position, style_)
}
