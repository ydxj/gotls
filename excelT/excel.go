package excelT

import (
	"fmt"
	
	"github.com/xuri/excelize/v2"
	
	"gitee.com/ydxj/gotls/baseT/collection/sliceT"
	"gitee.com/ydxj/gotls/baseT/osT"
)

type Row struct {
	Len       int
	RowIndex  int
	Position  string
	File      *excelize.File
	SheetName string
	CellList  []*Cell
}

type Sheet struct {
	Name  string
	Index int
	File  *excelize.File
	// 默认值是空的，读取的时候会填充（不必反复读）
	RowList []*Row
	// 默认值是空的，读取的时候会填充（不必反复读）
	MergeCellList []excelize.MergeCell
}

type Excel struct {
	Name string
	Path string
	File *excelize.File
}

// 读取Excel文件
func ReadExcel(path string) *Excel {
	file, err := excelize.OpenFile(path)
	if err != nil {
		panic("读取Excel文件错误")
	}
	return &Excel{
		Name: osT.GetFileName(path),
		Path: path,
		File: file,
	}
}

// 创建一个Excel文件
func CreateExcel(path string) *Excel {
	if !osT.FileIsExists(path) {
		file := excelize.NewFile()
		err := file.SaveAs(path)
		if err != nil {
			panic(fmt.Sprintf("创建Excel文件错误: %s", err))
		}
		return &Excel{
			Name: osT.GetFileName(path),
			Path: path,
			File: file,
		}
	} else {
		return ReadExcel(path)
	}
}

// 读取Sheet
func (e *Excel) ReadSheet(sheetName string) *Sheet {
	if !sliceT.StringInSlice(sheetName, e.File.GetSheetList()) {
		panic("Sheet不存在")
	}
	return &Sheet{
		Name:          sheetName,
		Index:         sliceT.GetIndex(sheetName, e.File.GetSheetList()),
		File:          e.File,
		MergeCellList: nil,
	}
}

// 读取Sheet
func (e *Excel) ReadSheetByIndex(index int) *Sheet {
	if index >= len(e.File.GetSheetList()) {
		panic("Sheet不存在")
	}
	return &Sheet{
		Name:          e.File.GetSheetList()[index],
		Index:         index,
		File:          e.File,
		MergeCellList: nil,
	}
}

// 获取Sheet列表
func (e *Excel) GetSheetList() []string {
	return e.File.GetSheetList()
}

// 读取全部数据
func (s *Sheet) ReadRows() []*Row {
	rows, err := s.File.Rows(s.Name)
	if err != nil {
		panic("读取行数据错误")
	}
	s.RowList = getRows(s.File, s.Name, rows, 1, -1)
	return s.RowList
}

// 读取标题行
func (s *Sheet) ReadTitleRow() []*Row {
	rows, err := s.File.Rows(s.Name)
	if err != nil {
		panic("读取行数据错误")
	}
	s.RowList = getRows(s.File, s.Name, rows, 1, 1)
	return s.RowList
}

// 读取指定范围的数据
func (s *Sheet) ReadRowsWithRange(start, end int) []*Row {
	rows, err := s.File.Rows(s.Name)
	if err != nil {
		panic("读取行数据错误")
	}
	s.RowList = getRows(s.File, s.Name, rows, start, end)
	return s.RowList
}

// 保存文件
func (e *Excel) Save() {
	_ = e.File.UpdateLinkedValue()
	_ = e.File.Save()
}

// 另存为文件
func (e *Excel) SaveAs(filename string) {
	_ = e.File.UpdateLinkedValue()
	_ = e.File.SaveAs(filename)
}

// 获取行
func getRows(file *excelize.File, sheet string, rows *excelize.Rows, startRow, endRow int) []*Row {
	rowIndex := 0
	rowMaxCount := 0
	rowList := make([]*Row, 0)
	for rows.Next() {
		rowIndex++
		if rowIndex < startRow {
			continue
		}
		if rowIndex > endRow && endRow != -1 && startRow <= endRow {
			break
		}
		if rowIndex%1000 == 0 {
			fmt.Println(fmt.Sprintf("GetRows: Row Just : %d", rowIndex))
		}
		colIndex := 0
		var cellList []*Cell
		row, _ := rows.Columns()
		if len(row) > rowMaxCount {
			rowMaxCount = len(row)
		}
		for i := len(row); i < rowMaxCount; i++ {
			row = append(row, "")
		}
		for _, data := range row {
			colIndex++
			cell := Cell{
				Value:     data,
				Position:  GetCellPosition(rowIndex, colIndex),
				RowIndex:  rowIndex,
				ColIndex:  colIndex,
				SheetName: sheet,
				File:      file,
			}
			cellList = append(cellList, &cell)
		}
		rowList = append(rowList, &Row{
			Len:       len(cellList),
			File:      file,
			SheetName: sheet,
			RowIndex:  rowIndex,
			CellList:  cellList,
			Position:  fmt.Sprintf("A%d", rowIndex),
		})
	}
	// 补齐之前的元素
	for _, row := range rowList {
		for i := len(row.CellList); i < rowMaxCount; i++ {
			row.CellList = append(row.CellList, &Cell{
				File:      file,
				SheetName: sheet,
				Value:     "",
				RowIndex:  row.RowIndex,
				ColIndex:  i + 1,
				Position:  GetCellPosition(row.RowIndex, i+1),
			})
		}
		row.Len = len(row.CellList)
	}
	return rowList
}
