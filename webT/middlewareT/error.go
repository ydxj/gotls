package middlewareT

import (
	"fmt"
	"net/http"
	
	"github.com/gin-gonic/gin"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

type CustomError struct {
	Code int
	Msg  string
	Log  string
}

// 错误处理
func ErrorHandler() gin.HandlerFunc {
	return gin.RecoveryWithWriter(nil, func(ctx *gin.Context, err any) {
		if e, ok := err.(CustomError); ok {
			logT.Log(fmt.Sprintf("msg: %s log: %s", e.Msg, e.Log))
			ctx.JSON(http.StatusOK, gin.H{
				"status_code": e.Code,
				"status_msg":  e.Msg,
			})
		} else {
			ctx.JSON(http.StatusOK, gin.H{
				"status_code": 500,
				"status_msg":  "unknown error, please contact the administrator",
			})
		}
		// 中断请求
		ctx.Abort()
	})
}
