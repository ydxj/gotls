package checkT

import (
	"strings"
)

// 检测车牌号是否合法
func CarCode(code string) bool {
	return carCodePattern.MatchString(strings.ToUpper(code))
}

// 检测姓名是否合法
func Name(name string) (bool, string) {
	// 判断是否还有常见符号
	if strings.ContainsAny(name, "【】（）、；’，。`~!@#$%^&*()_+-=|{}[]:;'<>,.?/ 　") {
		return false, "名称含有符号"
	}
	// 判断是否只含有中文
	if !namePattern.MatchString(name) {
		return false, "名称含有除中文以外的字符"
	}
	// 判断是否含有岗位关键词
	for _, s := range []string{
		"负责人", "法人", "科员", "科长", "书记", "主任",
		"局长", "所长", "园长", "馆长",
		"厅长", "会长", "队长", "校长",
		"镇长", "主席", "领导", "处长", "部长"} {
		if strings.Contains(name, s) {
			return false, "名称含有岗位关键词"
		}
	}
	// 判断是否以姓氏开头
	for _, lastname := range lastNames {
		// 如果以某个姓氏开头
		if strings.HasPrefix(name, lastname) {
			return true, "ok"
		}
	}
	// 悬而未决
	return false, "名称可能不以姓氏开头"
}

// 检测地址是否合法
func Address(address string) bool {
	for _, province := range provinceList {
		if strings.HasPrefix(address, province) {
			return addressRegexp.MatchString(address)
		}
	}
	return false
}
