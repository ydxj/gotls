package main

import (
	"fmt"
	
	"gitee.com/ydxj/gotls/baseT/osT"
)

func main() {
	// fmt.Print(osT.GetCmdInput("请输入0：", true))
	// fmt.Print(osT.GetCmdInput("请输入1：", true, "exit"))
	// fmt.Print(osT.GetCmdInput("请输入2：", false, "exit"))
	//
	name := osT.GetCmdArgs("name", "hello", "请输入名字：")
	age := osT.GetCmdArgsInt("age", 18, "请输入年龄：")
	// name := flag.String("name", "hello", "请输入名字：")
	// age := flag.String("age", "18", "请输入年龄：")
	osT.FlagParse()
	fmt.Println(*name, *age)
}
