package fyneT

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	
	"gitee.com/ydxj/gotls/baseT/numT"
)

// 分裂布局
func SplitLayout(offset float64, leading fyne.CanvasObject, trailing ...fyne.CanvasObject) fyne.CanvasObject {
	l := container.NewHSplit(leading, container.NewGridWithColumns(len(trailing), trailing...))
	l.SetOffset(offset)
	return l
}

func SplitLayoutII(canvasObject fyne.CanvasObject, canvasObjects ...fyne.CanvasObject) fyne.CanvasObject {
	obj := canvasObject
	offset := numT.Divide(1, 2)
	for i, object := range canvasObjects {
		obj = SplitLayout(1-offset, obj, object)
		offset = numT.Divide(1, max(1, 3+i))
	}
	return obj
}

type COffset struct {
	Offset       float64
	CanvasObject fyne.CanvasObject
}

func SplitLayoutIII(canvasObject COffset, canvasObjects ...COffset) fyne.CanvasObject {
	obj := canvasObject.CanvasObject
	offset := canvasObject.Offset
	totalOffset := offset
	for _, object := range canvasObjects {
		obj = SplitLayout(totalOffset/(totalOffset+object.Offset), obj, object.CanvasObject)
		totalOffset += object.Offset
	}
	return obj
}

func VSplitLayout(offset float64, leading fyne.CanvasObject, trailing ...fyne.CanvasObject) fyne.CanvasObject {
	l := container.NewHSplit(leading, container.NewGridWithColumns(len(trailing), trailing...))
	l.SetOffset(offset)
	l.Horizontal = false
	return l
}

func VSplitLayoutII(canvasObject fyne.CanvasObject, canvasObjects ...fyne.CanvasObject) fyne.CanvasObject {
	obj := canvasObject
	offset := numT.Divide(1, 2)
	for i, object := range canvasObjects {
		obj = VSplitLayout(1-offset, obj, object)
		offset = numT.Divide(1, max(1, 3+i))
	}
	return obj
}

func VSplitLayoutIII(canvasObject COffset, canvasObjects ...COffset) fyne.CanvasObject {
	obj := canvasObject.CanvasObject
	offset := canvasObject.Offset
	totalOffset := offset
	for _, object := range canvasObjects {
		obj = VSplitLayout(totalOffset/(totalOffset+object.Offset), obj, object.CanvasObject)
		totalOffset += object.Offset
	}
	return obj
}
