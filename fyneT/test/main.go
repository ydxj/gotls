package main

import (
	"time"
	
	"fyne.io/fyne/v2/widget"
	
	"gitee.com/ydxj/gotls/baseT/osT"
	"gitee.com/ydxj/gotls/fyneT"
)

func main() {
	osT.ExpiredCheck(time.Now().Add(1 * time.Second))
	_, win := fyneT.InitApp("TestGUI", fyneT.ResourceIconPng)
	c1 := widget.NewEntry()
	c1.SetText("C1")
	c2 := widget.NewEntry()
	c2.SetText("C2")
	c3 := widget.NewEntry()
	c3.SetText("C3")
	c4 := widget.NewEntry()
	c4.SetText("C4")
	c5 := widget.NewEntry()
	c5.SetText("C5")
	// win.SetContent(fyneT.SplitLayoutII(c1, c2, c3, c4, c5))
	win.SetContent(fyneT.VSplitLayoutIII(
		fyneT.COffset{CanvasObject: c1, Offset: 0.5},
		fyneT.COffset{CanvasObject: c2, Offset: 0.3},
		fyneT.COffset{CanvasObject: c3, Offset: 0.15},
		fyneT.COffset{CanvasObject: c4, Offset: 0.1},
		fyneT.COffset{CanvasObject: c5, Offset: 0.4}))
	fyneT.Show(win, 800, 400)
}
