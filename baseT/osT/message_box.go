//go:build windows

package osT

import (
	"os"
	"syscall"
	"time"
	"unsafe"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

const (
	MB_OK                = 0x00000000
	MB_OKCANCEL          = 0x00000001
	MB_ABORTRETRYIGNORE  = 0x00000002
	MB_YESNOCANCEL       = 0x00000003
	MB_YESNO             = 0x00000004
	MB_RETRYCANCEL       = 0x00000005
	MB_CANCELTRYCONTINUE = 0x00000006
	MB_ICONHAND          = 0x00000010
	MB_ICONQUESTION      = 0x00000020
	MB_ICONEXCLAMATION   = 0x00000030
	MB_ICONASTERISK      = 0x00000040
	MB_USERICON          = 0x00000080
	MB_ICONWARNING       = MB_ICONEXCLAMATION
	MB_ICONERROR         = MB_ICONHAND
	MB_ICONINFORMATION   = MB_ICONASTERISK
	MB_ICONSTOP          = MB_ICONHAND
	
	MB_DEFBUTTON1 = 0x00000000
	MB_DEFBUTTON2 = 0x00000100
	MB_DEFBUTTON3 = 0x00000200
	MB_DEFBUTTON4 = 0x00000300
)

func IntPtr(n int) uintptr {
	return uintptr(n)
}

func StrPtr(s string) uintptr {
	return uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(s)))
}

// https://learn.microsoft.com/zh-cn/windows/win32/api/winuser/nf-winuser-messagebox#return-value
func MessageBoxOK(title, text string) {
	user32 := syscall.NewLazyDLL("user32.dll")
	MessageBox := user32.NewProc("MessageBoxW")
	// 1 OK
	_, _, err := MessageBox.Call(IntPtr(0), StrPtr(text), StrPtr(title), MB_OK)
	if err != nil {
		logT.Error(err.Error())
	}
}

func MessageBoxYesNo(title, text string) bool {
	user32 := syscall.NewLazyDLL("user32.dll")
	MessageBox := user32.NewProc("MessageBoxW")
	// 6 是 7 否
	w, _, err := MessageBox.Call(IntPtr(0), StrPtr(text), StrPtr(title), MB_YESNO)
	if err != nil {
		logT.Error(err.Error())
		return false
	}
	if w == 6 {
		return true
	}
	return false
}

func MessageBoxYesNoCancel(title, text string) int {
	user32 := syscall.NewLazyDLL("user32.dll")
	MessageBox := user32.NewProc("MessageBoxW")
	// 6 是 7 否 2 取消
	w, _, err := MessageBox.Call(IntPtr(0), StrPtr(text), StrPtr(title), MB_YESNOCANCEL)
	if err != nil {
		logT.Error(err.Error())
		return -1
	}
	if w == 6 {
		return 1
	} else if w == 7 {
		return 0
	}
	return -1
}

// 检查是否过期
func ExpiredCheck(ti time.Time) {
	if ti.Before(time.Now()) {
		MessageBoxOK("ERROR", "版本已过期，请联系开发者获取最新版本")
		MessageBoxOK("INFO", "Author: YouDianXinJi")
		os.Exit(-1)
	}
}

// 检查是否过期（自定义消息）
func ExpiredCheckWithInfo(ti time.Time, infos [][]string) {
	if ti.Before(time.Now()) {
		for _, info := range infos {
			MessageBoxOK(info[0], info[1])
		}
		os.Exit(-1)
	}
}
