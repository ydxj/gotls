//go:build windows

package osT

import (
	"bufio"
	"fmt"
	"io"
	"os/exec"
	"strconv"
	"syscall"
	
	"golang.org/x/text/encoding/simplifiedchinese"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

type charset string

const (
	UTF8    = charset("UTF-8")
	GB18030 = charset("GB18030")
)

// 对字符进行转码
func ByteToString(byte []byte, charset charset) string {
	var str string
	switch charset {
	case GB18030:
		var decodeBytes, err = simplifiedchinese.GB18030.NewDecoder().Bytes(byte)
		if err != nil {
			logT.Error(err.Error())
		}
		str = string(decodeBytes)
	case UTF8:
		fallthrough
	default:
		str = string(byte)
	}
	return str
}

// 封装一个函数来执行命令
func ExecCommand(commandName string, params []string, f func(str string)) {
	// 执行命令
	cmd := exec.Command(commandName, params...)
	cmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	// 输出
	stderr, err := cmd.StderrPipe()
	if err != nil {
		logT.Error(err.Error())
	}
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		logT.Error(err.Error())
	}
	// 输出重定向
	if f != nil {
		StdOut(f, stderr, stdout)
	}
	// 执行cmd
	cmd.Start()
}

// 杀死进程
func KillCommand(pid int) {
	killCmd := exec.Command("taskkill", "/F", "/T", "/PID", strconv.Itoa(pid))
	killCmd.SysProcAttr = &syscall.SysProcAttr{HideWindow: true}
	_ = killCmd.Run()
}

func StdOut(f func(str string), out ...io.Reader) {
	for _, r := range out {
		go func(reader io.Reader) {
			in := bufio.NewScanner(reader)
			for in.Scan() {
				rst := fmt.Sprintf("%s", ByteToString(in.Bytes(), "GB18030"))
				f(rst)
			}
		}(r)
	}
}
