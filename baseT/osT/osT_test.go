package osT

import (
	"fmt"
	"testing"
	"time"
	
	"gitee.com/ydxj/gotls/baseT/testT"
)

func TestCMD(t *testing.T) {
	ExecCommand("ping", []string{"127.0.0.1"}, func(str string) {
		fmt.Println(">>> ", str)
	})
	time.Sleep(15 * time.Second)
}

func TestGetRootFile(t *testing.T) {
	fmt.Println(GetDirFile("E:/****", false, 2))
}

func TestCopyFile(t *testing.T) {
	CopyFile("./file.go", "./file_copy_v1.txt")
}

func TestFunc(t *testing.T) {
	testT.InitTestEnv()
	rst := GetDirFile(".", true, 2)
	fmt.Println(len(rst))
	rst2 := GetDirFile(".", true, -1)
	fmt.Println(len(rst2))
	
	// fmt.Println(GetFileName("./base.txt"))
	// fmt.Println(GetFileNameWithSuffix("./base.txt"))
	// fmt.Println(GetFileDir("./base.txt"))
	// fmt.Println(GetFileAbsDir("./base.txt"))
	// fmt.Println(GetFileSuffix("./base.txt"))
}
