package jsonT

import (
	"bytes"
	"encoding/json"
	"fmt"
	
	"gitee.com/ydxj/gotls/baseT/fileT"
	"gitee.com/ydxj/gotls/baseT/logT"
)

// 美化输出json数据
func fmtJson(data string) string {
	var prettyJSON bytes.Buffer
	if err := json.Indent(&prettyJSON, []byte(data), "", "    "); err != nil {
		logT.Error(err.Error())
		return "{}"
	} else {
		return prettyJSON.String()
	}
}

// 格式化输出json
func FmtPrintln(data any) {
	// 将args转换为json格式
	jsonStr, err := json.Marshal(data)
	if err != nil {
		logT.Error(err.Error())
	}
	fmt.Println(fmtJson(string(jsonStr)))
}

// 将json字符串转换为map
func JsonToMap(str string) map[string]any {
	var rst map[string]any
	err := json.Unmarshal([]byte(str), &rst)
	if err != nil {
		rst["ERROR"] = err.Error()
		logT.Error(err.Error())
	}
	return rst
}

// 将任意数据转换为json字符串
func AnyToJsonStr(data any) string {
	jsonStr, err := json.Marshal(data)
	if err != nil {
		logT.Error(err.Error())
	}
	return fmtJson(string(jsonStr))
}

// 将数据保存为json文件
func SaveDataToFile(filename string, data any) {
	jsonStr, err := json.Marshal(data)
	if err != nil {
		logT.Error(err.Error())
	}
	fileT.CreatFile(filename, fmtJson(string(jsonStr)))
}

// 从JsonFile中读取数据
func GetDataFromFile(filename string, data any) {
	jsonStr := fileT.ReadFile(filename)
	err := json.Unmarshal([]byte(jsonStr), data)
	if err != nil {
		logT.Error(err.Error())
	}
}
