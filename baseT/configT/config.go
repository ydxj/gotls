package configT

import (
	"os"
	"time"
	
	"github.com/spf13/viper"
	
	"gitee.com/ydxj/gotls/baseT/logT"
	"gitee.com/ydxj/gotls/baseT/osT"
)

type Config struct {
	Viper *viper.Viper
}

func NewConfig(filepath string, ty ...string) *Config {
	// 获取配置
	v := viper.New()
	// 设置读取的文件路径
	v.AddConfigPath(osT.GetFileDir(filepath))
	// 设置读取的文件名
	v.SetConfigName(osT.GetFileNameWithSuffix(filepath))
	// 设置文件的类型
	if len(ty) > 0 {
		v.SetConfigType(osT.GetFileSuffix(ty[0]))
	} else {
		v.SetConfigType(osT.GetFileSuffix(filepath)[1:])
	}
	// 尝试进行配置读取
	err := v.ReadInConfig()
	if err != nil {
		logT.Error(err.Error())
		panic("配置读取失败: " + err.Error())
	}
	return &Config{Viper: v}
}

func GenEncryptConfig(filepath string, key string) {
	encrypt(filepath, filepath+".age", key)
}

func NewEncryptConfig(filepath string, key string, ty ...string) *Config {
	// 读取加密文件
	tempConfig := time.Now().Format("temp-2006010205")
	decrypt(filepath, tempConfig, key)
	config := NewConfig(tempConfig, ty...)
	// 删除加密文件
	_ = os.Remove(tempConfig)
	return config
}

func NewYamlConfig(configPath string) *Config {
	return NewConfig(configPath)
}

func (config *Config) ReadTime(name string, defaultValue ...int) time.Time {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetTime(name)
}

func (config *Config) ReadBool(name string, defaultValue ...int) bool {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetBool(name)
}

func (config *Config) ReadInt(name string, defaultValue ...int) int {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetInt(name)
}

func (config *Config) ReadIntSlice(name string, defaultValue ...int) []int {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetIntSlice(name)
}

func (config *Config) ReadFloat64(name string, defaultValue ...int) float64 {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetFloat64(name)
}

func (config *Config) ReadInt32(name string, defaultValue ...int) int32 {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetInt32(name)
}

func (config *Config) ReadInt64(name string, defaultValue ...int) int64 {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetInt64(name)
}

func (config *Config) ReadString(name string, defaultValue ...string) string {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetString(name)
}

func (config *Config) ReadStringSlice(name string, defaultValue ...[]string) []string {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.GetStringSlice(name)
}

func (config *Config) ReadAny(name string, defaultValue ...any) any {
	if len(defaultValue) > 0 {
		config.Viper.SetDefault(name, defaultValue[0])
	}
	return config.Viper.Get(name)
}
