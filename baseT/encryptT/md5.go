package encryptT

import (
	"crypto/md5"
	
	"github.com/anaskhan96/go-password-encoder"
)

type Md5Config struct {
	SaltLen    int
	Iterations int
	KeyLen     int
}

var DefaultMd5Config = Md5Config{SaltLen: 10, Iterations: 10, KeyLen: 30}

// 生成Md5
func GenerateMd5(psd string) (encodedPsd, salt string) {
	options := &password.Options{SaltLen: DefaultMd5Config.SaltLen, Iterations: DefaultMd5Config.Iterations, KeyLen: DefaultMd5Config.KeyLen, HashFunction: md5.New}
	salt, encodedPsd = password.Encode(psd, options)
	return
}

// 验证Md5
func VerifyMd5(encodePsd, psd, salt string) bool {
	options := &password.Options{SaltLen: DefaultMd5Config.SaltLen, Iterations: DefaultMd5Config.Iterations, KeyLen: DefaultMd5Config.KeyLen, HashFunction: md5.New}
	check := password.Verify(psd, salt, encodePsd, options)
	return check
}
