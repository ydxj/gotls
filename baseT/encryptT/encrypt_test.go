package encryptT

import (
	"fmt"
	"testing"
)

func TestRSA(t *testing.T) {
	pub, pri := GenerateRSAKey(512)
	fmt.Println(string(pub))
	fmt.Println(string(pri))
	ciphertext := EncryptRSA("hello", pub)
	text := DecryptRSA(ciphertext, pri)
	fmt.Println(text)
}

func TestAES(t *testing.T) {
	c := EncryptAES("6PkaIzpSi4WfuQmw", "hello world")
	fmt.Println(c, DecryptAES("6PkaIzpSi4WfuQmw", c))
}

func TestHash(t *testing.T) {
	fmt.Println(HashSha1("hello world"))
	fmt.Println(HashSha256("hello world"))
	fmt.Println(HashSha512("hello world"))
}
