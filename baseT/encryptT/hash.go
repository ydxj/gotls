package encryptT

import (
	"crypto/sha1"
	"fmt"
	"io"
	
	"golang.org/x/crypto/sha3"
)

func HashSha1(data string) string {
	t := sha1.New()
	_, _ = io.WriteString(t, data)
	return fmt.Sprintf("%x", t.Sum(nil))
}

func HashSha256(data string) string {
	t := sha3.New256()
	_, _ = io.WriteString(t, data)
	return fmt.Sprintf("%x", t.Sum(nil))
}

func HashSha512(data string) string {
	t := sha3.New512()
	_, _ = io.WriteString(t, data)
	return fmt.Sprintf("%x", t.Sum(nil))
}
