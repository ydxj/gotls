package encryptT

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"errors"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

// 填充
func pkcs7Padding(data []byte, blockSize int) []byte {
	// 判断缺少几位长度。最少1，最多 blockSize
	padding := blockSize - len(data)%blockSize
	// 补足位数。把切片[]byte{byte(padding)}复制padding个
	padText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padText...)
}

// 填充的反向操作
func pkcs7UnPadding(data []byte) ([]byte, error) {
	length := len(data)
	if length == 0 {
		return nil, errors.New("encrypted string error")
	}
	// 获取填充的个数
	unPadding := int(data[length-1])
	return data[:(length - unPadding)], nil
}

func EncryptAES(key string, data string) string {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		logT.Error(err.Error())
		panic(err)
	}
	blockSize := block.BlockSize()
	encryptBytes := pkcs7Padding([]byte(data), blockSize)
	crypto := make([]byte, len(encryptBytes))
	blockMode := cipher.NewCBCEncrypter(block, []byte(key[:blockSize]))
	blockMode.CryptBlocks(crypto, encryptBytes)
	return string(crypto)
}

func DecryptAES(key string, data string) string {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		logT.Error(err.Error())
		panic(err)
	}
	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, []byte(key[:blockSize]))
	crypto := make([]byte, len(data))
	blockMode.CryptBlocks(crypto, []byte(data))
	crypto, err = pkcs7UnPadding(crypto)
	if err != nil {
	}
	return string(crypto)
}
