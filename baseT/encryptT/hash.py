import hashlib


def sha1_secret_str(s: str):
    sha = hashlib.sha1(s.encode('utf-8'))
    encrypts = sha.hexdigest()
    return encrypts


print(sha1_secret_str("c490d2d7-70bd-4c8e-ba2f-30449690f819-Jiudan@1021"))
