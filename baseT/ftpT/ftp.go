package ftpT

import (
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"time"
	
	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

type RemoteClient struct {
	client *sftp.Client
}

func NewSftpConnect(host string, port int, user, password string, timeout int) (client *RemoteClient, err error) {
	auth := make([]ssh.AuthMethod, 0)
	auth = append(auth, ssh.Password(password))
	clientConfig := &ssh.ClientConfig{
		User:    user,
		Auth:    auth,
		Timeout: time.Duration(timeout) * time.Millisecond,
		HostKeyCallback: func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			return nil
		},
	}
	sshClient, err := ssh.Dial("tcp", fmt.Sprintf("%s:%d", host, port), clientConfig) // 连接ssh
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	sftpClient, err := sftp.NewClient(sshClient)
	if err != nil {
		logT.Error(err.Error())
		return nil, err
	}
	return &RemoteClient{client: sftpClient}, nil
}

func (c *RemoteClient) ReadDir(remoteDirPath string) ([]os.FileInfo, error) {
	remoteDirPath = filepath.ToSlash(remoteDirPath)
	return c.client.ReadDir(remoteDirPath)
}

func (c *RemoteClient) Stat(remotePath string) (os.FileInfo, error) {
	remotePath = filepath.ToSlash(remotePath)
	return c.client.Stat(remotePath)
}

func (c *RemoteClient) Chtimes(remotePath string, atime time.Time, mtime time.Time) error {
	remotePath = filepath.ToSlash(remotePath)
	return c.client.Chtimes(remotePath, atime, mtime)
}

func (c *RemoteClient) DownloadFile(remoteFilePath string, localFilePath string) error {
	remoteFilePath = filepath.ToSlash(remoteFilePath)
	srcFile, err := c.client.Open(remoteFilePath)
	if err != nil {
		return err
	}
	// 获取远程文件信息（包含修改时间、访问时间等）
	remoteFileInfo, err := srcFile.Stat()
	if err != nil {
		return err
	}
	// 读取文件
	bytes, err := io.ReadAll(srcFile)
	if err != nil {
		return err
	}
	// 获取目录
	dir := filepath.Dir(localFilePath)
	_, err = os.Stat(dir)
	if os.IsNotExist(err) {
		_ = os.MkdirAll(dir, 0755)
	}
	err = os.WriteFile(localFilePath, bytes, 0644)
	// 设置本地文件的访问时间和修改时间与远程文件一致
	err = os.Chtimes(localFilePath, remoteFileInfo.ModTime(), remoteFileInfo.ModTime())
	if err != nil {
		return err
	}
	return err
}

func (c *RemoteClient) walkDir(remoteDirPath string, rst *[]string, getDir bool) {
	remoteDirPath = filepath.ToSlash(remoteDirPath)
	files, err := c.ReadDir(remoteDirPath)
	if err != nil {
		logT.Error("Read Dir Error: " + err.Error())
		return
	}
	for _, f := range files {
		if f.IsDir() {
			if getDir {
				*rst = append(*rst, filepath.Join(remoteDirPath, f.Name()))
			}
			c.walkDir(remoteDirPath+"/"+f.Name(), rst, getDir)
		} else {
			*rst = append(*rst, filepath.Join(remoteDirPath, f.Name()))
		}
	}
}

func (c *RemoteClient) WalkDir(remoteDirPath string, getDir bool) []string {
	remoteDirPath = filepath.ToSlash(remoteDirPath)
	rst := make([]string, 0)
	c.walkDir(remoteDirPath, &rst, getDir)
	return rst
}

func (c *RemoteClient) FileExistsOnRemote(remoteFilePath string) bool {
	remoteFilePath = filepath.ToSlash(remoteFilePath)
	_, err := c.client.Stat(remoteFilePath)
	return err == nil
}

func (c *RemoteClient) RenameRemoteFile(oldRemoteFilePath, newRemoteFilePath string) error {
	oldRemoteFilePath = filepath.ToSlash(oldRemoteFilePath)
	newRemoteFilePath = filepath.ToSlash(newRemoteFilePath)
	err := c.client.Rename(oldRemoteFilePath, newRemoteFilePath)
	return err
}

func (c *RemoteClient) RemoveRemoteFile(remoteFilePath string) error {
	remoteFilePath = filepath.ToSlash(remoteFilePath)
	err := c.client.Remove(remoteFilePath)
	return err
}

func (c *RemoteClient) CompareFile(remoteFilePath, localFilePath string) bool {
	remoteFilePath = filepath.ToSlash(remoteFilePath)
	// 判断文件是否存在
	localFileInfo, err := os.Stat(localFilePath)
	// 本地文件不存在，直接返回false
	if os.IsNotExist(err) || err != nil {
		return false
	}
	// 比较远程文件信息（一般来说，比对文件名、文件大小、修改时间即可）
	remoteFileInfo, err := c.Stat(remoteFilePath)
	if os.IsNotExist(err) || err != nil {
		return false
	}
	if remoteFileInfo.Name() == localFileInfo.Name() && remoteFileInfo.Size() == localFileInfo.Size() && remoteFileInfo.ModTime() == localFileInfo.ModTime() {
		return true
	}
	return false
}

func (c *RemoteClient) UploadFile(localFilePath, remoteFilePath string) error {
	remoteFilePath = filepath.ToSlash(remoteFilePath)
	// 打开本地文件
	localFile, err := os.Open(localFilePath)
	if err != nil {
		return err
	}
	defer func(localFile *os.File) {
		_ = localFile.Close()
	}(localFile)
	// 获取文件信息（包含修改时间、访问时间等）
	localFileInfo, err := localFile.Stat()
	if err != nil {
		return err
	}
	// 创建远程目录
	err = c.MakeRemoteDir(filepath.ToSlash(filepath.Dir(remoteFilePath)))
	if err != nil {
		return err
	}
	// 创建远程文件
	remoteFile, err := c.client.Create(remoteFilePath)
	if err != nil {
		return err
	}
	defer func(remoteFile *sftp.File) {
		_ = remoteFile.Close()
	}(remoteFile)
	// 从本地文件读取内容并写入远程文件
	_, err = io.Copy(remoteFile, localFile)
	if err != nil {
		return err
	}
	// 设置远程文件的访问时间和修改时间与本地文件一致
	err = c.Chtimes(remoteFilePath, localFileInfo.ModTime(), localFileInfo.ModTime())
	return nil
}

func (c *RemoteClient) MakeRemoteDir(remoteDirPath string) error {
	remoteDirPath = filepath.ToSlash(remoteDirPath)
	// 检查远程目录是否已存在
	_, err := c.client.Stat(remoteDirPath)
	if err == nil {
		return nil
	}
	if !os.IsNotExist(err) {
		return err
	}
	// 尝试创建远程目录
	err = c.client.Mkdir(remoteDirPath)
	if err != nil {
		return err
	}
	return nil
}

func (c *RemoteClient) Close() error {
	return c.client.Close()
}

func (c *RemoteClient) Client() *sftp.Client {
	return c.client
}
