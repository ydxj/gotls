package ftpT

import (
	"fmt"
	"testing"
)

func TestUploadFile(t *testing.T) {
	client, err := NewSftpConnect("192.168.20.40", 22, "root", "*******", 3000)
	fmt.Println(client, err)
	defer client.Close()
	// 创建目录
	err = client.MakeRemoteDir("/tmp/a")
	fmt.Println(err)
	err = client.UploadFile("./ftp.go", "\\tmp\\a\\ftp.go")
	fmt.Println(err)
}
