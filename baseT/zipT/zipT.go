package zipT

import (
	"archive/zip"
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

func Zip(source, zipFile string, comment string) error {
	// 读取文件夹的名称
	folderName := filepath.Base(source)
	// 创建一个新的 zip 文件
	zipFileWriter, err := os.Create(zipFile)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	defer func(zipFileWriter *os.File) {
		_ = zipFileWriter.Close()
	}(zipFileWriter)
	// 创建 zip 写入器
	zipWriter := zip.NewWriter(zipFileWriter)
	defer func(zipWriter *zip.Writer) {
		_ = zipWriter.Close()
	}(zipWriter)
	// 设置备注
	if comment != "" {
		comment = time.Now().Format("note: 2006-01-02 15:04:05")
		err = zipWriter.SetComment(comment)
		if err != nil {
			logT.Error(err.Error())
			return err
		}
	}
	// 获取源文件的绝对路径
	absSource, err := filepath.Abs(source)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	// 遍历文件夹并添加到 zip 文件中
	return filepath.Walk(absSource, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			logT.Error(err.Error())
			return err
		}
		// 计算文件相对路径
		relPath, err := filepath.Rel(absSource, path)
		if err != nil {
			logT.Error(err.Error())
			return err
		}
		relPath = filepath.Join(folderName, relPath)
		if err != nil {
			logT.Error(err.Error())
			return err
		}
		// 如果是目录，则在 zip 文件中创建一个目录项
		if info.IsDir() {
			if relPath != "." {
				_, err := zipWriter.Create(relPath + "/")
				if err != nil {
					logT.Error(err.Error())
					return err
				}
			}
			return nil
		}
		// 否则将文件添加到 zip 文件
		return addFileToZip(zipWriter, path, relPath)
	})
}

func addFileToZip(zipWriter *zip.Writer, file string, relPath string) error {
	f, err := os.Open(file)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	defer func(f *os.File) {
		_ = f.Close()
	}(f)
	// 在 zip 文件中创建该文件
	writer, err := zipWriter.Create(relPath)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	// 将文件内容写入 zip
	_, err = io.Copy(writer, f)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	return nil
}

func UnZip(zipFile, destDir string) error {
	r, err := zip.OpenReader(zipFile)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	defer func(r *zip.ReadCloser) {
		_ = r.Close()
	}(r)
	wg := sync.WaitGroup{}
	for _, f := range r.File {
		wg.Add(1)
		go func(rf *zip.File, w *sync.WaitGroup) {
			defer w.Done()
			if err := unzipFile(rf, destDir); err != nil {
				logT.Error(err.Error())
			}
		}(f, &wg)
	}
	wg.Wait()
	return nil
}

func unzipFile(f *zip.File, destDir string) error {
	filename := f.Name
	filePath := filepath.Join(destDir, filename)
	// 创建文件夹
	if f.FileInfo().IsDir() {
		return os.MkdirAll(filePath, os.ModePerm)
	}
	// 创建文件的父目录
	if err := os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
		logT.Error(err.Error())
		return err
	}
	// 打开文件
	file, err := f.Open()
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	defer func(file io.ReadCloser) {
		_ = file.Close()
	}(file)
	// 创建文件
	outFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	defer func(outFile *os.File) {
		_ = outFile.Close()
	}(outFile)
	// 将文件内容写入
	_, err = io.Copy(outFile, file)
	if err != nil {
		logT.Error(err.Error())
		return err
	}
	return nil
}
