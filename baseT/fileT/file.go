package fileT

import (
	"encoding/hex"
	"hash"
	"os"
	
	"gitee.com/ydxj/gotls/baseT/logT"
	"gitee.com/ydxj/gotls/baseT/osT"
)

// 创建文件
func CreatFile(filename string, text string) {
	// 判断文件是否存在
	if osT.FileIsExists(filename) {
		logT.Error("文件已存在")
		return
	}
	file, err := os.Create(filename)
	if err != nil {
		logT.Error(err.Error())
	}
	defer file.Close()
	_, err = file.WriteString(text)
	if err != nil {
		logT.Error(err.Error())
	}
}

// 读取文件
func ReadFile(filename string) string {
	byteContent, err := os.ReadFile(filename)
	if err != nil {
		logT.Error(err.Error())
	}
	content := string(byteContent)
	return content
}

// 写入文件
func WriteFile(filepath, content string) {
	// 写入文件（清除文件内容，覆盖文件写入）
	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0666)
	defer file.Close()
	if err != nil {
		logT.Error(err.Error())
	}
	_, err = file.WriteString(content)
	if err != nil {
		logT.Error(err.Error())
	}
}

// 在文件最后追加内容
func AppendFile(filepath, content string) {
	// 写入文件
	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	defer file.Close()
	if err != nil {
		logT.Error(err.Error())
	}
	_, err = file.WriteString(content)
	if err != nil {
		logT.Error(err.Error())
	}
}

// 生成文件指纹
func GetFilenameFingerprint(binaryData []byte, hasher func() hash.Hash) string {
	h := hasher()
	h.Write(binaryData)
	return hex.EncodeToString(h.Sum(nil))
}
