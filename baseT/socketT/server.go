package socketT

import (
	"fmt"
	"net"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

func connProcess(conn net.Conn, deal func(str string) string) {
	for {
		buffer := make([]byte, 1024)
		n, err := conn.Read(buffer)
		if err != nil {
			logT.Error("读取客户端消息失败:" + err.Error())
			return
		}
		msg := string(buffer[:n])
		// 客户端主动关闭
		if msg == "exit" {
			_ = conn.Close()
			return
		}
		// 处理消息
		rst := deal(msg)
		// 服务端主动关闭
		if rst == "exit" {
			_ = conn.Close()
			return
		}
		// 向客户端发送确认收到的消息
		_, err = conn.Write([]byte(rst))
		if err != nil {
			logT.Error("向客户端发送确认消息失败:" + err.Error())
			return
		}
	}
}

func ReceiveMsg(ln net.Listener, deal func(str string) string) {
	for {
		// 接受客户端连接
		conn, err := ln.Accept()
		if err != nil {
			logT.Error("接受连接失败:" + err.Error())
			return
		} else {
			logT.Log(fmt.Sprintf("accept success ip是=%v\n", conn.RemoteAddr()))
		}
		go connProcess(conn, deal)
	}
}

func NewServer(port int) net.Listener {
	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		logT.Error("监听端口失败:" + err.Error())
		return nil
	}
	return ln
}

func SendMsg(conn net.Conn, msg string) string {
	// 发送消息给服务端
	_, err := conn.Write([]byte(msg))
	if err != nil {
		logT.Error("发送消息失败:" + err.Error())
		return ""
	}
	// 读取服务端返回的确认消息
	buffer := make([]byte, 1024)
	n, err := conn.Read(buffer)
	if err != nil {
		logT.Error("读取服务端返回的消息失败:" + err.Error())
		return ""
	}
	ack := string(buffer[:n])
	return ack
}

func NewClient(address string, port int) net.Conn {
	// 连接服务端
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", address, port))
	if err != nil {
		logT.Error("连接服务端失败:" + err.Error())
		return nil
	}
	return conn
}
