package imageT

import (
	"image/png"
	"os"
	
	"github.com/kbinani/screenshot"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

func Screenshot(filePath string) {
	img, err := screenshot.CaptureDisplay(0)
	if err != nil {
		logT.Error(err.Error())
	}
	file, err := os.Create(filePath)
	defer file.Close()
	if err != nil {
		logT.Error(err.Error())
	}
	err = png.Encode(file, img)
	if err != nil {
		logT.Error(err.Error())
	}
}
