package imageT

import (
	"fmt"
	"testing"
	
	"github.com/gin-gonic/gin"
	
	"gitee.com/ydxj/gotls/webT/captchaT"
)

func TestCaptcha(t *testing.T) {
	Screenshot("./Screenshot.png")
	id, b64s, answer := captchaT.GenerateCaptcha()
	fmt.Println(id, answer)
	captchaT.SaveCaptcha(b64s, "./test.png")
	fmt.Println(captchaT.VerifyCaptcha(id, answer))
	router := gin.Default()
	captchaT.InitCaptcha(router)
	router.Run(":10039")
}
