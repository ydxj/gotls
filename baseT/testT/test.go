package testT

import (
	"fmt"
	"os"
	"path/filepath"
	
	"gitee.com/ydxj/gotls/baseT/logT"
)

// 查找 go.mod
func findGoModPath() (string, error) {
	dir, err := os.Getwd()
	if err != nil {
		logT.Error(err.Error())
		return "", err
	}
	for {
		goModPath := filepath.Join(dir, "go.mod")
		if _, err := os.Stat(goModPath); err == nil {
			absPath, err := filepath.Abs(dir)
			if err != nil {
				return "", err
			}
			return absPath, nil
		}
		parentDir := filepath.Dir(dir)
		if parentDir == dir {
			// 已经到达根目录，仍未找到 go.mod 文件
			return "", fmt.Errorf("go.mod file not found")
		}
		dir = parentDir
	}
}

func InitTestEnv() {
	path, err := findGoModPath()
	if err != nil {
		logT.Error(err.Error())
	}
	_ = os.Chdir(path)
}
