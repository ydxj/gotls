package sliceT

// 切片过滤
func Filter[T comparable](slice []T, condition func(T) bool) []T {
	var filtered []T
	for _, item := range slice {
		if condition(item) {
			filtered = append(filtered, item)
		}
	}
	return filtered
}

// 判断切片是否为空
func IsEmpty(array []string) bool {
	if len(array) == 0 {
		return true
	}
	return false
}
