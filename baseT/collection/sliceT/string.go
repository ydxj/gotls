package sliceT

import "strings"

// 全等
func StringInSlice[T comparable](val T, stringsSlice []T) bool {
	for _, str := range stringsSlice {
		if val == str {
			return true
		}
	}
	return false
}

// 传入的 val 应该是比较短的部分 如 六安市 In ["安徽省六安市“] True
func StringInSliceA(val string, stringsSlice []string) bool {
	for _, str := range stringsSlice {
		if strings.Contains(str, val) {
			return true
		}
	}
	return false
}

// 传入的 val 应该是比较长的 如 安徽省六安市 In [”六安市“] True
func StringInSliceB(val string, stringsSlice []string) bool {
	for _, str := range stringsSlice {
		if strings.Contains(val, str) {
			return true
		}
	}
	return false
}

// 去重函数 - 变成集合
func ToSet[T comparable](strings []T) []T {
	result := make([]T, 0)
	seen := make(map[T]struct{})
	for _, s := range strings {
		if _, ok := seen[s]; !ok {
			result = append(result, s)
			seen[s] = struct{}{}
		}
	}
	return result
}

// 判断字符串是否在列表中
func ContainStr(val string, valueList []string) bool {
	for _, s := range valueList {
		if s == val {
			return true
		}
	}
	return false
}

// 判断字符串在列表中的位置
func GetIndex[T comparable](val T, stringsSlice []T) int {
	for i, t := range stringsSlice {
		if val == t {
			return i
		}
	}
	return -1
}
