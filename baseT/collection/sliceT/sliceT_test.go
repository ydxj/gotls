package sliceT

import (
	"fmt"
	"testing"
)

func TestToSet(t *testing.T) {
	ss := ToSet([]string{"123", "456", "789", "456"})
	fmt.Println(ss)
	ii := ToSet([]int{123, 456, 789, 456})
	fmt.Println(ii)
	ff := ToSet([]float64{123, 456, 789, 456})
	fmt.Println(ff)
}
