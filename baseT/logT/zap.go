package logT

import (
	"fmt"
	"io"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
	
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
	
	"gitee.com/ydxj/gotls/baseT/colorT"
)

var (
	DebugLevel = zapcore.DebugLevel
	InfoLevel  = zapcore.InfoLevel
	WarnLevel  = zapcore.WarnLevel
	ErrorLevel = zapcore.ErrorLevel
)

// 可以输出 结构化日志、非结构化日志。性能差于 zap.Logger
var sugaredLogger *zap.SugaredLogger

// 用于定义日志文件大小
func getLogWriter(filename string) io.Writer {
	return &lumberjack.Logger{
		Filename:   filename,
		MaxSize:    200,  // 最大M数，超过则切割
		MaxBackups: 15,   // 最大文件保留数，超过就删除最老的日志文件
		MaxAge:     365,  // 保存365天
		Compress:   true, // 是否压缩
	}
}

// InitLog 初始化日志 logger
func InitLog(logDirPath string, logLevel ...zapcore.Level) {
	level := zapcore.InfoLevel
	if len(logLevel) > 0 {
		level = logLevel[0]
	}
	config := zapcore.EncoderConfig{
		MessageKey:   "Info",                      // 结构化（json）输出：msg的key
		LevelKey:     "Level",                     // 结构化（json）输出：日志级别的key（INFO，WARN，ERROR等）
		TimeKey:      "Time",                      // 结构化（json）输出：时间的key（INFO，WARN，ERROR等）
		CallerKey:    "Call",                      // 结构化（json）输出：打印日志的文件对应的Key
		EncodeLevel:  zapcore.CapitalLevelEncoder, // 将日志级别转换成大写（INFO，WARN，ERROR等）
		EncodeCaller: zapcore.ShortCallerEncoder,  // 采用短文件路径编码输出（test/main.go:14）
		EncodeTime: func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendString(t.Format("2006-01-02 15:04:05"))
		}, // 输出的时间格式
		EncodeDuration: func(d time.Duration, enc zapcore.PrimitiveArrayEncoder) {
			enc.AppendInt64(int64(d) / 1000000)
		},                         // 耗时
		ConsoleSeparator: "\t|\t", // 控制台分隔符
	}
	debugLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		// 等于debug级别的日志会被写入
		return lvl == zapcore.DebugLevel
	})
	infoLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		// 等于Info级别的日志会被写入
		return lvl == zapcore.InfoLevel
	})
	warnLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		// 大于Warn级别的日志会被写入
		return lvl >= zapcore.WarnLevel
	})
	// 获取io.Writer
	logWriter := getLogWriter(fmt.Sprintf("%s/log.log", logDirPath))
	debuWriter := getLogWriter(fmt.Sprintf("%s/debug.log", logDirPath))
	infoWriter := getLogWriter(fmt.Sprintf("%s/info.log", logDirPath))
	warnWriter := getLogWriter(fmt.Sprintf("%s/warn.log", logDirPath))
	// 实现多个输出
	core := zapcore.NewTee(
		zapcore.NewCore(zapcore.NewConsoleEncoder(config), zapcore.AddSync(logWriter), level),
		zapcore.NewCore(zapcore.NewConsoleEncoder(config), zapcore.AddSync(debuWriter), debugLevel),
		zapcore.NewCore(zapcore.NewConsoleEncoder(config), zapcore.AddSync(infoWriter), infoLevel),
		zapcore.NewCore(zapcore.NewConsoleEncoder(config), zapcore.AddSync(warnWriter), warnLevel),
		zapcore.NewCore(zapcore.NewConsoleEncoder(config), zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), level),
	)
	logger := zap.New(core, zap.AddCaller(), zap.AddStacktrace(zap.InfoLevel))
	sugaredLogger = logger.Sugar()
	// 监听程序退出以刷新缓存到文件中
	go Sync()
}

func Sync() {
	sigChan := make(chan os.Signal, 1)
	// 将指定的信号发送到sigChan通道
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	// 等待接收信号
	_ = <-sigChan
	// 将缓存中的日志写入到文件中
	_ = sugaredLogger.Sync()
	os.Exit(0)
}

func Error(msg string) {
	colorT.PrintlnError(msg)
	if sugaredLogger != nil {
		sugaredLogger.Errorf(msg)
	}
}

func Warn(msg string) {
	if sugaredLogger != nil {
		sugaredLogger.Warnf(msg)
	}
}

func Info(msg string) {
	if sugaredLogger != nil {
		sugaredLogger.Infof(msg)
	}
}

func Log(msg string) {
	if strings.Contains(strings.ToLower(msg), "warn") {
		Warn(msg)
	} else if strings.Contains(strings.ToLower(msg), "error") {
		Error(msg)
	} else if strings.Contains(strings.ToLower(msg), "debug") {
		Error(msg)
	} else {
		Info(msg)
	}
}
