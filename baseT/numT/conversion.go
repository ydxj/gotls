package numT

import (
	"regexp"
	"slices"
	"strconv"
	"strings"
)

var numberCn2an = map[string]int{
	"零": 0,
	"〇":  0,
	"一": 1,
	"壹": 1,
	"幺": 1,
	"二": 2,
	"贰": 2,
	"两": 2,
	"三": 3,
	"叁": 3,
	"四": 4,
	"肆": 4,
	"五": 5,
	"伍": 5,
	"六": 6,
	"陆": 6,
	"七": 7,
	"柒": 7,
	"八": 8,
	"捌": 8,
	"九": 9,
	"玖": 9,
}

var unitCn2an = map[string]int{
	"十": 10,
	"拾": 10,
	"百": 100,
	"佰": 100,
	"千": 1000,
	"仟": 1000,
	"万": 10000,
	"亿": 100000000,
}
var numberAn2cn = []string{"零", "一", "二", "三", "四", "五", "六", "七", "八", "九"}
var unitAn2cn = []string{"", "十", "百", "千", "万", "十万", "百万", "千万", "亿", "十亿", "百亿", "千亿", "万亿", "亿亿"}

// Cn2an 将中文数字转换为阿拉伯数字，不支持小数
func Cn2Int(cn string) int {
	var out int
	var cnList []string
	for _, b := range cn {
		slices.Insert(cnList, 0, string(b))
	}
	if _, ok := unitCn2an[cnList[len(cnList)-1]]; ok {
		cnList = append(cnList, "一")
	}
	weight := 1
	big := false
	for _, c := range cnList {
		if a, ok := numberCn2an[c]; ok {
			out += a * weight
			weight *= 10
			big = false
		} else if a, ok := unitCn2an[c]; ok {
			// 保权
			if big {
				weight *= a
			} else {
				weight = a
			}
			big = true
		} else {
			return 0
		}
	}
	return out
}

// An2cn 阿拉伯数字转汉字
func Int2Cn(an int) string {
	var out string
	str := strconv.Itoa(an)
	for i, a := range str {
		if string(a) == "0" {
			out += "零"
			continue
		}
		if index := len(str) - 1 - i; index < len(unitAn2cn) {
			a_n, _ := strconv.Atoi(string(a))
			out += numberAn2cn[a_n] + unitAn2cn[index]
		} else {
			return ""
		}
	}
	re := regexp.MustCompile(`零{2,}`)
	out = strings.Trim(re.ReplaceAllString(out, "零"), "零")
	return out
}
