package timeT

import (
	"time"
	
	"gitee.com/ydxj/gotls/baseT/logT"
	"gitee.com/ydxj/gotls/baseT/numT"
)

//  获取时间戳
func GetTimestampByTime(ti time.Time) int {
	return int(ti.Unix())
}

// 时间转字符串
func FormatTime(ti time.Time, format ...string) string {
	if len(format) != 1 {
		return ti.Format("2006-01-02 15:04:05")
	} else {
		return ti.Format(format[0])
	}
}

// 获取当前时间
func GetNowTime(format ...string) string {
	return FormatTime(time.Now(), format...)
}

// 字符串转时间
func StrToTime(ti string, format ...string) time.Time {
	var err error
	t := time.Time{}
	if len(format) != 1 {
		t, err = time.Parse("2006-01-02 15:04:05", ti)
	} else {
		t, err = time.Parse(format[0], ti)
	}
	if err != nil {
		logT.Error(err.Error())
	}
	return t
}

// 计算时间差
func CalcTime(startTi, endTi time.Time) int {
	return int(endTi.Sub(startTi).Milliseconds())
}

// 计算时间差
func SinceTime(endTi time.Time, isSecond bool) float64 {
	if isSecond {
		// 转为秒
		return numT.Divide(int(time.Since(endTi).Milliseconds()), 1000, 4)
	}
	// 转为毫秒
	return float64(time.Since(endTi).Milliseconds())
}
